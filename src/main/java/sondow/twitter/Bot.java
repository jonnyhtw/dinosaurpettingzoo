package sondow.twitter;

import java.security.SecureRandom;
import twitter4j.Status;

/**
 * The main application logic.
 */
public class Bot {

    /**
     * The object that does the tweeting.
     */
    private Tweeter tweeter;

    private SecureRandom random;

    /**
     * Constructs a new bot with the specified Tweeter, for unit testing.
     *
     * @param tweeter the object that tweets
     */
    Bot(Tweeter tweeter, SecureRandom random) {
        this.tweeter = tweeter;
        this.random = random;
    }

    /**
     * Constructs a new bot.
     */
    Bot() {
        this(new Tweeter(), new SecureRandom());
    }

    /**
     * Performs the application logic.
     *
     * @return the posted tweet
     */
    public Status go() {

        Zoo zoo = new ZooBuilder(random).build();
        String message = zoo.toString();
        Status tweet = tweeter.tweet(message);
        System.out.println(message);
        return tweet;
    }

    public static void main(String[] args) {
        Bot bot = new Bot(null, new SecureRandom());
        for (int i = 0; i < 500; i++) {
            bot.go();
        }
    }
}
