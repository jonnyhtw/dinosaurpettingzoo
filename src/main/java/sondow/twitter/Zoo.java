package sondow.twitter;

public class Zoo {

    private Grid grid;

    public Zoo(Grid grid) {
        this.grid = grid;
    }

    public String toString() {
        return grid.toString();
    }
}
