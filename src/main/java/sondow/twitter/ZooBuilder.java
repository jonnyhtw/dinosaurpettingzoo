package sondow.twitter;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static sondow.twitter.Chars.COMMON_ANIMALS;
import static sondow.twitter.Chars.FEATURES;
import static sondow.twitter.Chars.FENCE_BEAM;
import static sondow.twitter.Chars.FENCE_POST;
import static sondow.twitter.Chars.IDEOGRAPHIC_SPACE;
import static sondow.twitter.Chars.RARE_ANIMALS;
import static sondow.twitter.Chars.UNCOMMON_ANIMALS;
import static sondow.twitter.Chars.VERY_RARE_ANIMALS;

public class ZooBuilder {

    private SecureRandom random;

    public ZooBuilder(SecureRandom random) {
        this.random = random;
    }

    public Zoo build() {

        // Pick how many different kinds of common animalTypes there will be.
        List<String> animalTypes = new ArrayList<String>();

        // Usually around 2 types of common animalTypes
        Integer animalCommonsCount = pickOneFrom(Arrays
                .asList(1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4));
        while (animalTypes.size() < animalCommonsCount) {
            String animal = pickOneFrom(COMMON_ANIMALS);
            if (!animalTypes.contains(animal)) {
                animalTypes.add(animal);
            }
        }

        // At least one uncommon animal type should show up about once every 4 tweets.
        if (random.nextInt(4) == 1) {
            String uncommon1 = pickOneFrom(UNCOMMON_ANIMALS);
            animalTypes.add(uncommon1);
            // A second uncommon animal type should show up a third of the time uncommons are
            // present.
            int animalTypeCountWithOneUncommon = animalTypes.size();
            if (random.nextInt(3) == 2) {
                List<String> remainingUncommons = new ArrayList<>();
                for (String uncommon : UNCOMMON_ANIMALS) {
                    if (!animalTypes.contains(uncommon)) {
                        remainingUncommons.add(uncommon);
                    }
                }
                String uncommon2 = pickOneFrom(remainingUncommons);
                while (animalTypes.size() <= animalTypeCountWithOneUncommon) {
                    if (!animalTypes.contains(uncommon2)) {
                        animalTypes.add(uncommon2);
                    }
                }
            }
        }

        // One rare animal type should show up about once every 30 tweets.
        if (random.nextInt(30) == 17) {
            animalTypes.add(pickOneFrom(RARE_ANIMALS));
        }

        // One very rare animal type should show up about once every 60 tweets.
        if (random.nextInt(60) == 19) {
            animalTypes.add(pickOneFrom(VERY_RARE_ANIMALS));
        }

        int rows = 7;
        int cols = 9;
        String blank = IDEOGRAPHIC_SPACE;
        Grid grid = new Grid(rows, cols, blank);
        // Replace top and bottom rows with fences.
        String initialFence = FENCE_POST + FENCE_BEAM + FENCE_POST;
        String laterFence = FENCE_BEAM + FENCE_POST;
        for (int i = 0; i < cols - 1; i++) {
            String fence = (i == 0) ? initialFence : laterFence;
            grid.put(0, i, fence);
            grid.put(rows - 1, i, fence);
        }

        int minIndividuals = 4;
        int maxIndividuals = (int) (((double) (rows - 2)) * ((double) (cols - 2)) * 0.25d);
        int totalIndividualsPlaced = 0;

        // Place each intended animal type once, in an unoccupied space.
        for (String animalType : animalTypes) {
            Cell cell = pickRandomEmptyNonEdgeCell(grid);
            cell.setContents(animalType);
            grid.put(cell);
            totalIndividualsPlaced++;
        }
        int rangeSize = maxIndividuals - minIndividuals;
        int desiredTotalIndividuals = random.nextInt(rangeSize) + minIndividuals;
        while (totalIndividualsPlaced < desiredTotalIndividuals) {
            Cell cell = pickRandomEmptyNonEdgeCell(grid);
            String animalType = pickOneFrom(animalTypes);
            cell.setContents(getSmallPersonalSpace() + animalType);
            grid.put(cell);
            totalIndividualsPlaced++;
        }

        // A non-animal feature should show up once every 20 tweets.
        List<String> featureTypes = new ArrayList<String>();
        if (random.nextInt(20) == 11) {
            featureTypes.add(pickOneFrom(FEATURES));
        }
        for (String feature : featureTypes) {
            Cell cell = pickRandomEmptyNonEdgeCell(grid);
            cell.setContents(getSmallPersonalSpace() + feature);
            grid.put(cell);
        }
        return new Zoo(grid);
    }

    private Cell pickRandomEmptyNonEdgeCell(Grid grid) {
        int row = -1;
        int col = -1;
        String cellContents = "Not yet blank character";
        while (!cellContents.equals(grid.getInit())) {
            // Range of spaces to place animalTypes is non-edge spaces.
            // No animals at far sides nor top nor bottom.
            row = random.nextInt(grid.getRowCount() - 2) + 1;

            // We don't want two consecutive blank rows because Twitter will compress
            // them down to one blank row, so look for any double blank rows and pick one of them
            // if available.

            List<Integer> consecutiveBlankRows = grid.getConsecutiveBlankRows();
            if (consecutiveBlankRows.size() >= 2) {
                row = consecutiveBlankRows.get(1);
            }

            col = random.nextInt(grid.getColCount() - 2) + 1;
            cellContents = grid.getCellContents(row, col);
            Cell cell = grid.getCell(row, col);
            List<Cell> orthogonalNeighbors = grid.getOrthogonalNeighborsOf(cell);
        }

        return new Cell(row, col, cellContents);
    }

    private <T> T pickOneFrom(List<T> list) {
        return list.get(random.nextInt(list.size()));
    }

    private String getSmallPersonalSpace() {
        int jitter = random.nextInt(4);
        String personalSpace = Chars.EM_SPACE;
        if (jitter == 1) {
            personalSpace = Chars.THIN_SPACE;
        } else if (jitter == 2) {
            personalSpace = Chars.THREE_PER_EM_SPACE;
        } else if (jitter == 3) {
            personalSpace = Chars.EN_SPACE;
        }
        return personalSpace;
    }
}
