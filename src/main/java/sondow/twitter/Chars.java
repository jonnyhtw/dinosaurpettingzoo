package sondow.twitter;

import java.util.Arrays;
import java.util.List;

/**
 * The cast of characters.
 *
 * @author @JoeSondow
 */
public class Chars {

    public static final List<String> COMMON_ANIMALS = Arrays.asList(
            "🐇", "🐐", "🐄", "🐖", "🐓", "🐥", "🐑", "🐏");
    public static final List<String> UNCOMMON_ANIMALS = Arrays.asList(
            "🐎", "🐃", "🐪", "🐫", "🦔", "🐂", "🐈", "🐕", "🐢", "🐍", "🦃", "🦆",
            "🐿");
    public static final List<String> RARE_ANIMALS = Arrays.asList("🦩",
            "🦉", "🦢", "🦇", "🦎");
    public static final List<String> VERY_RARE_ANIMALS = Arrays.asList(
            "🦖", "🦕", "🐉", "🐅", "🐆", "🐊");

    // Wait until 2022 to add these 2018 emojis.
    // Common: llama 🦙
    // Uncommon: kangaroo 🦘, peacock 🦚
    // Very rare: microbe 🦠

    public static final List<String> FEATURES = Arrays.asList("⛲", "🗿", "🌱", "🌳", "🌴",
            "🍄", "🌵");

    public static final String FENCE_POST = "│"; // box drawing character vertical line
    //    public static final String FENCE_POST = "|"; // pipe
    public static final String FENCE_BEAM = "＝"; // full width equal sign
    //    public static final String FENCE = "="; // equals sign
    //    public static final String FENCE = "➖"; // emoji minus
    public static final String IDEOGRAPHIC_SPACE = "\u3000";
//    public static final String NONBREAKINGSPACE = "\u00A0";
    //    public static final String VARIATION_SELECTOR_15 = "\uFE0E";
    //    public static final String BRAILLE_PATTERN_BLANK = "\u2800";
    public static final String EN_SPACE = "\u2002";
    public static final String EM_SPACE = "\u2003";
    public static final String THREE_PER_EM_SPACE = "\u2004";
    public static final String THIN_SPACE = "\u2009";
//    public static final String HAIR_SPACE = "\u200a";
}
