package sondow.twitter

import java.security.SecureRandom
import spock.lang.Specification
import twitter4j.Status

class BotSpec extends Specification {

    def "go should make tweeter post a tweet"() {
        setup:
        Tweeter tweeter = Mock()
        SecureRandom random = new SecureRandom([0x0, 0x1, 0x3] as byte[])
        Bot bot = new Bot(tweeter, random)
        Status status = Mock()
        String message = '' +
                '│＝│＝│＝│＝│＝│＝│＝│＝│　\n' +
                '　　　　　　🐏　　\n' +
                '　　🐖 🐏　　　　　\n' +
                '　　 🌵　　　　　　\n' +
                '　　　 🐏　🐄　　　\n' +
                '　　　　　　　　　\n' +
                '│＝│＝│＝│＝│＝│＝│＝│＝│　'


        when:
        Status result = bot.go()

        then:
        1 * tweeter.tweet(message) >> status
        result == status
        0 * _._
    }
}
